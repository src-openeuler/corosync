# Conditionals
# Invoke "rpmbuild --without <feature>" or "rpmbuild --with <feature>"
# to disable or enable specific features
%bcond_with watchdog
%bcond_with monitoring
%bcond_without snmp
%bcond_without dbus
%bcond_without systemd
%bcond_without xmlconf
%bcond_without nozzle
%bcond_without vqsim
%bcond_without runautogen
%bcond_without userflags

%global gitver %{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}
%global gittarver %{?numcomm:.%{numcomm}}%{?alphatag:-%{alphatag}}%{?dirty:-%{dirty}}

Name:           corosync
Summary:        The Corosync Cluster Engine and Application Programming Interfaces
Version:        3.1.8
Release:        11
License:        BSD-3-Clause
URL:            http://corosync.github.io/corosync/
Source0:        http://build.clusterlabs.org/corosync/releases/%{name}-%{version}%{?gittarver}.tar.gz
Patch0:         Fix-up-the-library-versions-files.patch
Patch1:         Report-crypto-errors-back-to-cfg-reload.patch
Patch2:         Fix-building-of-rust-for-release.patch
Patch3:         totem-Fix-reference-links.patch
Patch4:         backport-Move-corosync-notifyd-policy-file-into-datadir.patch
Patch5:         backport-rust-Improve-Rust-bindings.patch
Patch6:         backport-rust-tests-return-errors-and-don-t-hang.patch
Patch7:         backport-man-Improve-quorum-provider-formatting.patch
Patch8:         backport-man-fix-a-typo-in-cpg_model_initialize.patch
Patch9:         backport-main-support-lock-pid-file-arg.patch
Patch10: 		backport-rust-fix-clippy-warning-in-rust-1.81.patch
Patch11:		backport-rust-Update-to-latest-standards.patch
Patch12:		backport-totem-Use-uint64_t-type-and-QB_TIME_NS_IN_MSEC.patch
Patch13:		backport-totem-Use-proper-timestamp-type-for-token-warning.patch
Patch14:		backport-stats-Store-token-rx-and-tx-timestamps-as-64-bit.patch
Patch15: 		backport-totemsrp-Fix-orf_token-stats.patch
Patch16:		backport-totemknet-Handle-new-knet2-datafd-API.patch
# Runtime bits
# The automatic dependency overridden in favor of explicit version lock
Requires:       corosynclib = %{version}-%{release}

# Support crypto reload
Requires:       libknet1 >= 1.18
# NSS crypto plugin should be always installed
Requires:       libknet1-crypto-nss-plugin >= 1.18

# Build bits
BuildRequires:  gcc
BuildRequires:  groff
BuildRequires:  libqb-devel
BuildRequires:  libknet1-devel >= 1.18
BuildRequires:  zlib-devel
%if %{with runautogen}
BuildRequires:  autoconf automake libtool
%endif
%if %{with monitoring}
BuildRequires:  libstatgrab-devel
%endif
%if %{with snmp}
BuildRequires:  net-snmp-devel
%endif
%if %{with dbus}
BuildRequires:  dbus-devel
%endif
%if %{with nozzle}
BuildRequires:  libnozzle1-devel
%endif
%if %{with systemd}
%{?systemd_requires}
BuildRequires:  systemd
BuildRequires:  systemd-devel
%else
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/chkconfig
%endif
%if %{with xmlconf}
Requires:       libxslt
%endif
%if %{with vqsim}
BuildRequires:  readline-devel
%endif
BuildRequires:  make

%prep
%autosetup -n %{name}-%{version}%{?gittarver} -p1

%build
%if %{with runautogen}
./autogen.sh
%endif

%{configure} \
%if %{with watchdog}
	--enable-watchdog \
%endif
%if %{with monitoring}
	--enable-monitoring \
%endif
%if %{with snmp}
	--enable-snmp \
%endif
%if %{with dbus}
	--enable-dbus \
%endif
%if %{with systemd}
	--enable-systemd \
%endif
%if %{with xmlconf}
	--enable-xmlconf \
%endif
%if %{with nozzle}
	--enable-nozzle \
%endif
%if %{with vqsim}
	--enable-vqsim \
%endif
%if %{with userflags}
	--enable-user-flags \
%endif
	--with-initddir=%{_initrddir} \
	--with-systemddir=%{_unitdir} \
	--docdir=%{_docdir}

%make_build

%install
%make_install

%if %{with dbus}
mkdir -p -m 0700 %{buildroot}/%{_sysconfdir}/dbus-1/system.d
install -m 644 %{_builddir}/%{name}-%{version}%{?gittarver}/conf/corosync-signals.conf %{buildroot}/%{_datadir}/dbus-1/system.d/corosync-signals.conf
%endif

## tree fixup
# drop static libs
rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la
# drop docs and html docs for now
rm -rf %{buildroot}%{_docdir}/*
# /etc/sysconfig/corosync-notifyd
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -m 644 tools/corosync-notifyd.sysconfig.example \
   %{buildroot}%{_sysconfdir}/sysconfig/corosync-notifyd
# /etc/sysconfig/corosync
install -m 644 init/corosync.sysconfig.example \
   %{buildroot}%{_sysconfdir}/sysconfig/corosync

%description
This package contains the Corosync Cluster Engine Executive, several default
APIs and libraries, default configuration files, and an init script.

%post
%if %{with systemd} && 0%{?systemd_post:1}
%systemd_post corosync.service
%else
if [ $1 -eq 1 ]; then
	/sbin/chkconfig --add corosync || :
fi
%endif

%preun
%if %{with systemd} && 0%{?systemd_preun:1}
%systemd_preun corosync.service
%else
if [ $1 -eq 0 ]; then
	/sbin/service corosync stop &>/dev/null || :
	/sbin/chkconfig --del corosync || :
fi
%endif

%postun
%if %{with systemd} && 0%{?systemd_postun:1}
%systemd_postun corosync.service
%endif

%files
%doc LICENSE
%{_sbindir}/corosync
%{_sbindir}/corosync-keygen
%{_sbindir}/corosync-cmapctl
%{_sbindir}/corosync-cfgtool
%{_sbindir}/corosync-cpgtool
%{_sbindir}/corosync-quorumtool
%{_sbindir}/corosync-notifyd
%{_bindir}/corosync-blackbox
%if %{with xmlconf}
%{_bindir}/corosync-xmlproc
%dir %{_datadir}/corosync
%{_datadir}/corosync/xml2conf.xsl
%{_mandir}/man8/corosync-xmlproc.8*
%{_mandir}/man5/corosync.xml.5*
%endif
%dir %{_sysconfdir}/corosync
%dir %{_sysconfdir}/corosync/uidgid.d
%config(noreplace) %{_sysconfdir}/corosync/corosync.conf.example
%config(noreplace) %{_sysconfdir}/sysconfig/corosync-notifyd
%config(noreplace) %{_sysconfdir}/sysconfig/corosync
%config(noreplace) %{_sysconfdir}/logrotate.d/corosync
%if %{with dbus}
%{_datadir}/dbus-1/system.d/corosync-signals.conf
%endif
%if %{with snmp}
%{_datadir}/snmp/mibs/COROSYNC-MIB.txt
%endif
%if %{with systemd}
%{_unitdir}/corosync.service
%{_unitdir}/corosync-notifyd.service
%else
%{_initrddir}/corosync
%{_initrddir}/corosync-notifyd
%endif
%dir %{_localstatedir}/lib/corosync
%dir %{_localstatedir}/log/cluster
%{_mandir}/man7/corosync_overview.7*
%{_mandir}/man8/corosync.8*
%{_mandir}/man8/corosync-blackbox.8*
%{_mandir}/man8/corosync-cmapctl.8*
%{_mandir}/man8/corosync-keygen.8*
%{_mandir}/man8/corosync-cfgtool.8*
%{_mandir}/man8/corosync-cpgtool.8*
%{_mandir}/man8/corosync-notifyd.8*
%{_mandir}/man8/corosync-quorumtool.8*
%{_mandir}/man5/corosync.conf.5*
%{_mandir}/man5/votequorum.5*
%{_mandir}/man7/cmap_keys.7*

# library
#
%package -n corosynclib
Summary: The Corosync Cluster Engine Libraries

%description -n corosynclib
This package contains corosync libraries.

%files -n corosynclib
%doc LICENSE
%{_libdir}/libcfg.so.*
%{_libdir}/libcpg.so.*
%{_libdir}/libcmap.so.*
%{_libdir}/libquorum.so.*
%{_libdir}/libvotequorum.so.*
%{_libdir}/libsam.so.*
%{_libdir}/libcorosync_common.so.*

%ldconfig_scriptlets -n corosynclib

%package -n corosynclib-devel
Summary: The Corosync Cluster Engine Development Kit
Requires:       corosynclib = %{version}-%{release}
Requires:       pkgconfig
Provides:       corosync-devel = %{version}

%description -n corosynclib-devel
This package contains include files and man pages used to develop using
The Corosync Cluster Engine APIs.

%files -n corosynclib-devel
%doc LICENSE
%dir %{_includedir}/corosync/
%{_includedir}/corosync/corodefs.h
%{_includedir}/corosync/cfg.h
%{_includedir}/corosync/cmap.h
%{_includedir}/corosync/corotypes.h
%{_includedir}/corosync/cpg.h
%{_includedir}/corosync/hdb.h
%{_includedir}/corosync/sam.h
%{_includedir}/corosync/quorum.h
%{_includedir}/corosync/votequorum.h
%{_libdir}/libcfg.so
%{_libdir}/libcpg.so
%{_libdir}/libcmap.so
%{_libdir}/libquorum.so
%{_libdir}/libvotequorum.so
%{_libdir}/libsam.so
%{_libdir}/libcorosync_common.so
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man3/cpg_*3*
%{_mandir}/man3/quorum_*3*
%{_mandir}/man3/votequorum_*3*
%{_mandir}/man3/sam_*3*
%{_mandir}/man3/cmap_*3*

%if %{with vqsim}
%package -n corosync-vqsim
Summary: The Corosync Cluster Engine - Votequorum Simulator
Requires:       corosynclib = %{version}-%{release}
Requires:       pkgconfig

%description -n corosync-vqsim
A command-line simulator for the corosync votequorum subsystem.
It uses the same code as the corosync quorum system but forks
them into subprocesses to simulate nodes.
Nodes can be added and removed as well as partitioned (to simulate
network splits)

%files -n corosync-vqsim
%doc LICENSE
%{_bindir}/corosync-vqsim
%{_mandir}/man8/corosync-vqsim.8*
%endif

%changelog
* Mon Dec 09 2024 bixiaoyan <bixiaoyan@kylinos.cn> - 3.1.8-11
- totemsrp: Fix orf_token stats
- totemknet: Handle new knet2 datafd API

* Fri Dec 06 2024 bixiaoyan <bixiaoyan@kylinos.cn> - 3.1.8-10
- totem: Use uint64_t type and QB_TIME_NS_IN_MSEC
- totem: Use proper timestamp type for token warning
- stats: Store token rx and tx timestamps as 64-bit

* Thu Dec 05 2024 bixiaoyan <bixiaoyan@kylinos.cn> - 3.1.8-9
- rust: fix clippy warning in rust 1.81
- rust: Update to latest standards

* Mon Oct 21 2024 bizhiyuan <bizhiyuan@kylinos.cn> - 3.1.8-8
- man: Improve quorum provider formatting 
- man: fix a typo in cpg_model_initialize
- main support lock pid file arg 

* Thu Sep 19 2024 bizhiyuan <bizhiyuan@kylinos.cn> - 3.1.8-7
- rust: remove rust bindings 
- rust: tests return errors and don't hang

* Thu Jun 06 2024 zouzhimin <zouzhimin@kylinos.cn> - 3.1.8-6
- Move corosync-notifyd policy file into $(datadir)/dbus-1/system.d and add yaml file

* Fri Mar 15 2024 zouzhimin <zouzhimin@kylinos.cn> - 3.1.8-5
- totem: Fix reference links

* Fri Mar 01 2024 zouzhimin <zouzhimin@kylinos.cn> - 3.1.8-4
- configure: Fix building of rust for release

* Tue Feb 27 2024 zouzhimin <zouzhimin@kylinos.cn> - 3.1.8-3
- Report crypto errors back to cfg reload

* Tue Feb 27 2024 zouzhimin <zouzhimin@kylinos.cn> - 3.1.8-2
- Fix up the library .versions files

* Fri Nov 24 2023 zouzhimin <zouzhimin@kylinos.cn> - 3.1.8-1
- update package to version 3.1.8

* Tue Nov 7 2023 bixiaoyan <bixiaoyan@kylinos.cn> - 3.1.7-5
- Fix integer overflow

* Thu Oct 26 2023 zouzhimin <zouzhimin@kylinos.cn> - 3.1.7-4
- Fail to start if knet ping timers are invalid

* Tue Aug 22 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 3.1.7-3
- Use knet TRACE logging level if available 

* Tue Jul 11 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 3.1.7-2
- Fix build error

* Wed May 31 2023 liupei <liupei@kylinos.cn> - 3.1.7-1
- update package to version 3.1.7

* Fri Feb 03 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 3.1.5-1
- Update package to version 3.1.5

* Thu Feb 17 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 3.0.4-1
- upgrade to 3.0.4

* Fri Oct 30 2020 jiangxinyu <jiangxinyu@kylinos.cn> - 3.0.2-5
- change the use_vmguestlib 1 to use_vmguestlib 0

* Mon Apr 13 2020 houjian<jian.hou@kylinos.cn> - 3.0.2-4.1
- Init corosync project

