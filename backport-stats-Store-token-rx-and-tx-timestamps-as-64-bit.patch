From 37858299358c10d1b67e6b3d3d0c686e2fb90273 Mon Sep 17 00:00:00 2001
From: Jan Friesse <jfriesse@redhat.com>
Date: Tue, 22 Oct 2024 18:43:15 +0200
Subject: [PATCH 04/10] stats: Store token rx and tx timestamps as 64-bit

Token rx and tx timestamps were computed and stored as 32-bit unsigned
integer but substracted in other parts of code from 64-bit integer.
Result was, that node with uptime larger than 49.71 days
(2^32/(1000*60*60*24)) reported wrong numbers for
stats.srp.time_since_token_last_received and in log message during long
pause (function timer_function_orf_token_warning).

Solution is to store rx and tx data as 64-bit integer.

Fixes #761

Signed-off-by: Jan Friesse <jfriesse@redhat.com>
Reviewed-by: Christine Caulfield <ccaulfie@redhat.com>
---
 exec/totemsrp.c                     | 5 ++---
 include/corosync/totem/totemstats.h | 4 ++--
 2 files changed, 4 insertions(+), 5 deletions(-)

diff --git a/exec/totemsrp.c b/exec/totemsrp.c
index 63463a53..2a0ad569 100644
--- a/exec/totemsrp.c
+++ b/exec/totemsrp.c
@@ -770,10 +770,9 @@ static int pause_flush (struct totemsrp_instance *instance)
 static int token_event_stats_collector (enum totem_callback_token_type type, const void *void_instance)
 {
 	struct totemsrp_instance *instance = (struct totemsrp_instance *)void_instance;
-	uint32_t time_now;
-	unsigned long long nano_secs = qb_util_nano_current_get ();
+	uint64_t time_now;
 
-	time_now = (nano_secs / QB_TIME_NS_IN_MSEC);
+	time_now = (qb_util_nano_current_get() / QB_TIME_NS_IN_MSEC);
 
 	if (type == TOTEM_CALLBACK_TOKEN_RECEIVED) {
 		/* incr latest token the index */
diff --git a/include/corosync/totem/totemstats.h b/include/corosync/totem/totemstats.h
index 51e604cf..3e42086c 100644
--- a/include/corosync/totem/totemstats.h
+++ b/include/corosync/totem/totemstats.h
@@ -45,8 +45,8 @@ typedef struct {
 } totemnet_stats_t;
 
 typedef struct {
-	uint32_t rx;
-	uint32_t tx;
+	uint64_t rx;
+	uint64_t tx;
 	int backlog_calc;
 } totemsrp_token_stats_t;
 
-- 
2.25.1

